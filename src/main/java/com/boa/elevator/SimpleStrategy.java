package com.boa.elevator;

import java.util.List;

public class SimpleStrategy implements Strategy {

	
	@Override
	public void traceRoute(List<Trip> trips) {

		StringBuilder route = new StringBuilder();

		if (trips == null || trips.size() == 0) {
			return;
		}

		for (Trip trip : trips) {
			int totalDistance = 0;
			int startingFloor = trip.getStartingFloor();
			int lastFloor = startingFloor;
			route.append(startingFloor).append(" ");

			for (Segment segment : trip.getSegments()) {

				int pickUpFloor = segment.getPickUpFloor();
				int dropOffFloor = segment.getDropOffFloor();

				totalDistance += Math.abs(lastFloor - pickUpFloor);
				totalDistance += Math.abs(pickUpFloor - dropOffFloor);
				route.append(pickUpFloor).append(" ");
				route.append(dropOffFloor).append(" ");
				lastFloor = dropOffFloor;
			}

			route.append("( ").append(totalDistance).append(" )");
			route.append("\n");
		}

		System.out.println(route);
	}

}
