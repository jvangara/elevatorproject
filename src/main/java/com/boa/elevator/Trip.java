package com.boa.elevator;

import java.util.List;

/**
 * Models the trip of an elevator, showing starting floor, and the various segments 
 * 
 */
public class Trip {

	private int startingFloor;
	
	private List<Segment> segments;

	public Trip(){}
	
	public Trip(List<Segment> segments) {
		super();
		this.segments = segments;
	}

	public int getStartingFloor() {
		return startingFloor;
	}

	public void setStartingFloor(int startingFloor) {
		this.startingFloor = startingFloor;
	}

	public List<Segment> getSegments() {
		return segments;
	}

	public void setSegments(List<Segment> segments) {
		this.segments = segments;
	}
	
}
