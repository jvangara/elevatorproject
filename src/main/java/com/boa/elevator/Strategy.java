package com.boa.elevator;

import java.util.List;

public interface Strategy {

	void traceRoute(List<Trip> trips);
}
