package com.boa.elevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Models an Elevator and its function
 * 
 */

public class Elevator {
	
	private Strategy strategy;
	
	public Strategy getStrategy() {
		return strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	public static void main(String[] args) {
			
		File inputFile = new File(args[0]);
		String mode = args[1];		
		Elevator elevator = new Elevator();
		
		if("A".equalsIgnoreCase(mode)) {
			elevator.setStrategy(new SimpleStrategy());
		}
		
		if("B".equalsIgnoreCase(mode)){
			elevator.setStrategy(new EfficientStrategy());
		}
		
		List<Trip> trips = elevator.readInput(inputFile);
		elevator.traceRoute(trips);
	}
	
	public List<Trip> readInput(File file) {
		
		List<Trip> trips = new ArrayList<Trip>();
		
		try {
			Reader reader = new FileReader(file);
			BufferedReader br = new BufferedReader(reader);
			List<Trip> tripList = new ArrayList<Trip>();
			
			String trip;
			
			while((trip = br.readLine()) != null ){
				
				String[] tokens = trip.split(":");
				
				int startingFloor = Integer.parseInt(tokens[0]);
				String[] segments = tokens[1].split(",");
				List<Segment> segmentList = new ArrayList<>();
				
				for (String segment : segments) {
					String[] floors = segment.split("-");
					int pickUpFloor = Integer.parseInt(floors[0]);
					int dropOffFloor = Integer.parseInt(floors[1]);
							
					Segment sg = new Segment(pickUpFloor, dropOffFloor);
					segmentList.add(sg);
				}
			
				Trip tp = new Trip();
				tp.setStartingFloor(startingFloor);
				tp.setSegments(segmentList);
				trips.add(tp);
			}
		
			reader.close();
			br.close();
			return trips;
		} 
		catch (FileNotFoundException e) {
			System.out.println("File could not be found");
			return Collections.EMPTY_LIST;
		}
		catch (IOException e){
			System.out.println("File could not be read" );
			return Collections.EMPTY_LIST;
		}
		
	} 
	
	
	public void traceRoute(List<Trip> trips){		
		getStrategy().traceRoute(trips);
	}
	
		
	
}