package com.boa.elevator;

/**
 * Models a Segment of elevator trip from pickUp to drop Off
 * 
 */
public class Segment {
	
	private int pickUpFloor;
	private int dropOffFloor;
		
	public Segment(){}

	public Segment(int pickUpFloor, int dropOffFloor) {
		super();
		this.pickUpFloor = pickUpFloor;
		this.dropOffFloor = dropOffFloor;
	}

	public int getPickUpFloor() {
		return pickUpFloor;
	}
	
	public void setPickUpFloor(int pickUpFloor) {
		this.pickUpFloor = pickUpFloor;
	}
	
	public int getDropOffFloor() {
		return dropOffFloor;
	}
	
	public void setDropOffFloor(int dropOffFloor) {
		this.dropOffFloor = dropOffFloor;
	}
		
}
