package com.boa.elevator;

import java.util.List;
import java.util.TreeSet;

public class EfficientStrategy implements Strategy {

	private TreeSet<Integer> upwardFloors;
	private TreeSet<Integer> downwardFloors; 
	private int currentFloor = 0;
	private Direction direction = null;

	public void addFloor(int flr) {
		
	    if(flr < currentFloor) {
	        downwardFloors.add(flr);
	    } 
	    
	    if(flr >= currentFloor) {
	        upwardFloors.add(flr);
	    }
	}

	public int nextFloor() {
	    if(direction == Direction.DOWN) {
	        return downwardFloors.pollLast(); // highest floor, or null if empty
	    } else {
	        return upwardFloors.pollFirst(); // lowest floor, or null if empty
	    }
	}
	
	@Override
	public void traceRoute(List<Trip> trips){
		
		StringBuilder route = new StringBuilder();
		
		if(trips == null || trips.size() == 0){
			return;
		}
		
		for (Trip trip : trips) {
			
			upwardFloors = new TreeSet<>();
			downwardFloors = new TreeSet<>();
			
			int totalDistance = 0;
			int startingFloor = trip.getStartingFloor();
			currentFloor = startingFloor;
			route.append(currentFloor).append(" ");
			
			int firstPickUp = trip.getSegments().get(0).getPickUpFloor();
			int firstDropOff = trip.getSegments().get(0).getDropOffFloor();
			
			if(startingFloor > firstPickUp){
				direction = Direction.DOWN;
			}
			else if (startingFloor < firstPickUp){
				direction = Direction.UP;
			}
			else {
				if(firstPickUp > firstDropOff){
					direction = Direction.DOWN;
				}
				else{
					direction = Direction.UP;
				}
			}
			
			for(Segment segment : trip.getSegments()){				
				int pickUpFloor = segment.getPickUpFloor();
				int dropOffFloor = segment.getDropOffFloor();
				addFloor(pickUpFloor);
				addFloor(dropOffFloor);
			}
			
			if(direction == Direction.UP){
				for (Integer floor : upwardFloors) {
					totalDistance += Math.abs(floor - currentFloor); 
					route.append(floor).append(" ");
					currentFloor = floor;
				}
				
				for (Integer floor : downwardFloors) {
					totalDistance += Math.abs(floor - currentFloor); 
					route.append(floor).append(" ");
					currentFloor = floor;
				}
			}
			
			if(direction == Direction.DOWN){
				for (Integer floor : downwardFloors) {
					totalDistance += Math.abs(floor - currentFloor); 
					route.append(floor).append(" ");
					currentFloor = floor;
				}
				
				for (Integer floor : upwardFloors) {
					totalDistance += Math.abs(floor - currentFloor); 
					route.append(floor).append(" ");
					currentFloor = floor;
				}
			}
			
			route.append("( ").append(totalDistance).append(" )");
			route.append("\n");
		}
		
		System.out.println(route);
		
	}

}
