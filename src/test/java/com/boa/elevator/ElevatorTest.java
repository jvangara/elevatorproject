package com.boa.elevator;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class ElevatorTest {

	Elevator elevator = new Elevator();
	
	private static File file;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		file = new File("src\\test\\resources\\input.txt");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Test
	public void testReadFile(){
		List<Trip> trips = elevator.readInput(file);
		
		assertNotNull(trips);
	}

	@Test
	public void testTraceRoute(){
		List<Trip> trips = elevator.readInput(file);
		elevator.setStrategy(new SimpleStrategy());
		elevator.traceRoute(trips);
		assertNotNull(trips);
	}
		
} 
